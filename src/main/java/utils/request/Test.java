package utils.request;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import utils.request.account.Account;
import utils.request.account.AccountId;
import utils.request.payreq.PaymentRequest;
import utils.request.payreq.PaymentRequestId;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Test {
    private final List<PaymentRequest> paymentRequestList = new ArrayList<>();
    private final static int ACCOUNT_NUM = 2;
    private final static int PAYREQ_ID_NUM = 2;
    private final static int PAYREQ_NUM = 2;

    @Before
    public void setUp() {
        int date = 7;
        int prId = 0;
        for (int accId = ACCOUNT_NUM; accId > 0; --accId) {
            for (int prNum = 0; prNum < PAYREQ_ID_NUM; prNum++) {
                prId++;
                for (int pr = 0; pr < PAYREQ_NUM; pr++) {
                    AccountId accountId = new AccountId(accId);
                    Account account = new Account(accountId);
                    PaymentRequestId paymentRequestId = new PaymentRequestId(prId);
                    PaymentRequest paymentRequest = new PaymentRequest(paymentRequestId, account, date--);
                    paymentRequestList.add(paymentRequest);
                }
            }
        }
    }

    @org.junit.Test
    public void test() {
        for (PaymentRequest paymentRequest : paymentRequestList) {
            PaymentRequestGrouping.add(paymentRequest);
        }

        String expected = "{\"AccountId(id=1)\":{\"PaymentRequestId(id=3)\":[{\"paymentRequestId\":{\"id\":3}," +
                "\"account\":{\"accountId\":{\"id\":1}},\"date\":3},{\"paymentRequestId\":{\"id\":3},\"account\":{" +
                "\"accountId\":{\"id\":1}},\"date\":2}],\"PaymentRequestId(id=4)\":[{\"paymentRequestId\":{\"id\":4}," +
                "\"account\":{\"accountId\":{\"id\":1}},\"date\":1},{\"paymentRequestId\":{\"id\":4},\"account\":{" +
                "\"accountId\":{\"id\":1}},\"date\":0}]},\"AccountId(id=2)\":{\"PaymentRequestId(id=1)\":[{" +
                "\"paymentRequestId\":{\"id\":1},\"account\":{\"accountId\":{\"id\":2}},\"date\":7},{\"paymentRequestId" +
                "\":{\"id\":1},\"account\":{\"accountId\":{\"id\":2}},\"date\":6}],\"PaymentRequestId(id=2)\":[{" +
                "\"paymentRequestId\":{\"id\":2},\"account\":{\"accountId\":{\"id\":2}},\"date\":5},{\"paymentRequestId" +
                "\":{\"id\":2},\"account\":{\"accountId\":{\"id\":2}},\"date\":4}]}}";

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String json = gson.toJson(PaymentRequestGrouping.getGroupedMap());

        assertEquals(expected, json);
        System.out.println(json);
    }
}
