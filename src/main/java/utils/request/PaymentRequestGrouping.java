package utils.request;

import utils.request.account.AccountId;
import utils.request.payreq.PaymentRequest;
import utils.request.payreq.PaymentRequestId;

import java.util.*;

public class PaymentRequestGrouping {
    private static final Map<AccountId, Map<PaymentRequestId, List<PaymentRequest>>> groupedMap = new TreeMap<>();
    public static void add(PaymentRequest paymentRequest) {
        AccountId accountId = paymentRequest.getAccount().getAccountId();
        PaymentRequestId paymentRequestId = paymentRequest.getPaymentRequestId();
        if (groupedMap.containsKey(accountId)) {
            if (groupedMap.get(accountId).containsKey(paymentRequestId)) {
                groupedMap.get(accountId).get(paymentRequestId).add(paymentRequest);
            } else {
                ArrayList<PaymentRequest> paymentRequestList = new ArrayList<>();
                paymentRequestList.add(paymentRequest);
                groupedMap.get(accountId).put(paymentRequestId, paymentRequestList);
            }
        } else {
            Map<PaymentRequestId, List<PaymentRequest>> paymentRequestMap = new HashMap<>();
            ArrayList<PaymentRequest> paymentRequestList = new ArrayList<>();
            paymentRequestList.add(paymentRequest);
            paymentRequestMap.put(paymentRequestId, paymentRequestList);
            groupedMap.put(accountId, paymentRequestMap);
        }
    }

    public static Map<AccountId, Map<PaymentRequestId, List<PaymentRequest>>> getGroupedMap() {
        return groupedMap;
    }
}
