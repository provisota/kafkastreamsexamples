package utils.request.account;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AccountId implements Comparable<AccountId> {
    private int id;

    @Override
    public int compareTo(AccountId o) {
        return this.id - o.getId();
    }
}
