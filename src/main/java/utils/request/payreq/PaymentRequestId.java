package utils.request.payreq;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PaymentRequestId {
    private int id;
}
