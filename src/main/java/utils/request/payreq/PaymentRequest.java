package utils.request.payreq;

import lombok.*;
import utils.request.account.Account;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PaymentRequest {
    private PaymentRequestId paymentRequestId;
    private Account account;
    private int date;
}
