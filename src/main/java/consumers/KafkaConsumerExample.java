package consumers;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class KafkaConsumerExample {
    private static final String STRING_DESERIALIZER = StringDeserializer.class.getName();
    private static final String SERVERS_LIST = "localhost:9092";
    // private static final String SERVERS_LIST = "localhost:9092,localhost:9093,localhost:9094";
    private static final String GROUP_ID = "CountryCounter";
    private static final Properties CONSUMER_PROPS = new Properties();
    private static final List<String> TOPICS_LIST = Collections.singletonList("streams-example-output");

    public static void main(String[] args) {
        CONSUMER_PROPS.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS_LIST);
        CONSUMER_PROPS.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        // CONSUMER_PROPS.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        CONSUMER_PROPS.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, STRING_DESERIALIZER);
        CONSUMER_PROPS.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, STRING_DESERIALIZER);

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(CONSUMER_PROPS);
        consumer.subscribe(TOPICS_LIST);
        startConsuming(consumer);
    }

    private static void startConsuming(KafkaConsumer<String, String> consumer) {
        try {
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    System.out.printf("topic = %s, partition = %s, offset = %d, key = %s, value = %s\n",
                            record.topic(), record.partition(), record.offset(), record.key(), record.value());
                }
            }
        } finally {
            consumer.close();
        }
    }
}
