package consumers;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MultithreadedConsumer implements Runnable {
    private static final String STRING_DESERIALIZER = StringDeserializer.class.getName();
    private static final String SERVERS_LIST = "localhost:9092,localhost:9093,localhost:9094";
    private final KafkaConsumer<String, String> consumer;
    private final List<String> topics;
    private final String id;

    public MultithreadedConsumer(String id, String groupId, List<String> topics) {
        this.id = id;
        this.topics = topics;

        Properties props = new Properties();

        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS_LIST);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        //props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, STRING_DESERIALIZER);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, STRING_DESERIALIZER);
        this.consumer = new KafkaConsumer<>(props);
    }

    public static void main(String[] args) {
        int numConsumers = 3;
        String groupId = "consumer-group";
        List<String> topics = Collections.singletonList("streams-example-output");
        ExecutorService executor = Executors.newFixedThreadPool(numConsumers);

        final List<MultithreadedConsumer> consumers = new ArrayList<>();
        for (int i = 0; i < numConsumers; i++) {
            MultithreadedConsumer consumer = new MultithreadedConsumer("Consumer " + i, groupId, topics);
            consumers.add(consumer);
            executor.submit(consumer);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for (MultithreadedConsumer consumer : consumers) {
                consumer.shutdown();
            }
            executor.shutdown();
            try {
                executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }));
    }

    @Override
    public void run() {
        try {
            consumer.subscribe(topics);
            // main Consumer loop
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    System.out.printf("%s: topic = %s, partition = %s, offset = %d, key = %s, value = %s\n",
                            this.id, record.topic(), record.partition(), record.offset(), record.key(), record.value());
                }
            }
        } catch (WakeupException e) {
            // ignore for shutdown 
        } finally {
            consumer.close();
        }
    }

    public void shutdown() {
        consumer.wakeup();
    }
}
