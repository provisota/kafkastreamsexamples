/*
 * How to run:
 * 1 Start Zookeeper server
 *
 * 2 Start Kafka Server
 *
 * 3 Create topic 'streams-plaintext-input':
 * bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-plaintext-input
 *
 * 4 Create file-input.txt file with some number of plaintext rows
 *
 * 5 And run the producer to fetch data from file to the stream 'streams-plaintext-input'
 * bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic streams-plaintext-input < file-input.txt
 *
 * 6 Now inspect the output of the WordCount demo application by reading from its output topic:
 * bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic streams-wordcount-output --from-beginning --formatter kafka.tools.DefaultMessageFormatter --property print.key=true --property print.value=true --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
 *
 * 7 Run WordCount.java
 *
 */

package streams.wordcount;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class WordCountTopology {
    private static int keyCount;
    private static final String KAFKA_SERVER_URL = "localhost:9092";
    private static final String APPLICATION_ID = "streams-wordcount";
    private static final String INPUT_TOPIC_NAME = "streams-plaintext-input";
    private static final String QUERYABLE_STORE_NAME = "Counts";
    private static final String OUTPUT_TOPIC_NAME = "streams-wordcount-output";

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, APPLICATION_ID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_SERVER_URL);
        // Enable record cache of size 10 MB.
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 10 * 1024 * 1024L);
        // Set commit to output topic interval to 1 second.
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 1000);
        // Disable record cache, to commit records to output topic immediately
        // props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        final KStreamBuilder builder = new KStreamBuilder();
        KStream<String, String> sourceStream = builder.stream(INPUT_TOPIC_NAME);

        sourceStream.flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split("\\W+")))
                .groupBy((key, value) -> value)
                .count(QUERYABLE_STORE_NAME)
                .to(Serdes.String(), Serdes.Long(), OUTPUT_TOPIC_NAME);

        /*sourceStream.leftJoin(sourceStream,
                (sourceStream1, sourceStream2) -> sourceStream1 + " " + sourceStream2,
                JoinWindows.of(TimeUnit.MINUTES.toMillis(5)),
                Serdes.String(),
                Serdes.String(),
                Serdes.String())
                .flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split("\\W+")))
                .groupBy((key, value) -> value)
                .count(QUERYABLE_STORE_NAME)
                .to(Serdes.String(), Serdes.Long(), OUTPUT_TOPIC_NAME);*/

        final KafkaStreams streams = new KafkaStreams(builder, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
