/*
 * How to run:
 * 1 Start Zookeeper server
 *
 * 2 Start Kafka Server
 *
 * 3 Create topic 'streams-plaintext-input':
 * bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-plaintext-input
 *
 * 4 Create file-input.txt file with some number of plaintext rows
 *
 * 5 And run the producer to fetch data from file to the stream 'streams-plaintext-input'
 * bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic streams-plaintext-input < file-input.txt
 *
 * 6 Now inspect the output of the WordCount demo application by reading from its output topic:
 * bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic streams-linesplit-output --from-beginning --formatter kafka.tools.DefaultMessageFormatter --property print.key=true --property print.value=true --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer --property value.deserializer=org.apache.kafka.common.serialization.StringDeserializer
 *
 * 7 Run LineSplit.java
 *
 */

package streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class LineSplit {
    private static int keyCount;

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-linesplit");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        final KStreamBuilder builder = new KStreamBuilder();
        KStream<String, String> sourceStream = builder.stream("streams-plaintext-input");
        KStream<String, String> wordsStream = sourceStream.flatMapValues(value -> Arrays.asList(value.split("\\W+")));
        KStream<String, String> formattedWordsStream = wordsStream.map((key, value) ->
                new KeyValue<>(String.format("Record number: %s", keyCount++), String.format("Value of record: %s", value)));
        formattedWordsStream.to("streams-linesplit-output");

        final KafkaStreams streams = new KafkaStreams(builder, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
