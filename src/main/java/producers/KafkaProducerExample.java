package producers;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class KafkaProducerExample {
    private static final String STRING_SERIALIZER = StringSerializer.class.getName();
    private static final String SERVERS_LIST = "localhost:9092";
    private static final String TOPIC_NAME = "streams-example-input";
    private static final Properties PRODUCER_PROPS = new Properties();

    public static void main(String[] args) {
        PRODUCER_PROPS.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS_LIST);
        PRODUCER_PROPS.put("key.serializer", STRING_SERIALIZER);
        PRODUCER_PROPS.put("value.serializer", STRING_SERIALIZER);
        // PRODUCER_PROPS.put("max.block.ms", 50); // default is 60000

        KafkaProducer<String, String> producer = new KafkaProducer<>(PRODUCER_PROPS);
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            startProducing(producer, reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void startProducing(KafkaProducer<String, String> producer, BufferedReader reader) throws IOException {
        ProducerRecord<String, String> record;
        while (true) {
                System.out.println("Please input line (\"exit\" for exit): ");
                String line = reader.readLine();
                if ("exit".equalsIgnoreCase(line)) {
                    break;
                }
                record = new ProducerRecord<>(TOPIC_NAME, line);
                producer.send(record, new DemoProducerCallback());
        }
        System.out.println("BYE! ;)");
    }

    private static class DemoProducerCallback implements Callback {
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if (e != null) {
                e.printStackTrace();
            } else {
                System.out.println(String.format("Topic %s ,offset %d ,partition %d ,timestamp %d",
                        recordMetadata.topic(), recordMetadata.offset(), recordMetadata.partition(),
                        recordMetadata.timestamp()));
            }
        }
    }
}
