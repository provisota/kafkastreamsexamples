package producers;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class MultithreadedProducer extends Thread {
    private static final String STRING_SERIALIZER = StringSerializer.class.getName();
    private static final String SERVERS_LIST = "localhost:9092,localhost:9093,localhost:9094";
    private static final String TOPIC_NAME = "streams-example-input";
    private static final Properties PRODUCER_PROPS = new Properties();
    private static final Object LOCK = new Object();
    private static final int NUM_OF_THREADS = 5;
    private static volatile AtomicInteger threadCount = new AtomicInteger(0);
    private static volatile boolean closeAllThreads;
    private static volatile ProducingStrategy producingStrategy;
    private static BufferedReader reader;

    public static void main(String[] args) {
        PRODUCER_PROPS.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, SERVERS_LIST);
        PRODUCER_PROPS.put("key.serializer", STRING_SERIALIZER);
        PRODUCER_PROPS.put("value.serializer", STRING_SERIALIZER);
        // PRODUCER_PROPS.put("max.block.ms", 50); // default is 60000

        reader = new BufferedReader(new InputStreamReader(System.in));
        getProducingStrategy();

        IntStream.range(0, NUM_OF_THREADS).forEach(x -> {
            MultithreadedProducer multithreadedProducer = getInstance();
            multithreadedProducer.setName("multithreadedProducer" + (threadCount.get() - 1));
            multithreadedProducer.start();
        });
    }

    private static MultithreadedProducer getInstance() {
        threadCount.getAndIncrement();
        return new MultithreadedProducer();
    }

    @Override
    public void run() {
        KafkaProducer<String, String> producer = new KafkaProducer<>(PRODUCER_PROPS);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            if (producingStrategy == ProducingStrategy.SIMPLE_PRODUCING) {
                startSimpleProducing(producer, reader);
            } else if (producingStrategy == ProducingStrategy.READ_AND_PRODUCING) {
                startReadAndProducing(producer, reader);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            threadCount.getAndDecrement();
            if (threadCount.get() == 0) {
                try {
                    sleep(1000); // give some time for print log messages
                    reader.close();
                    System.out.printf("reader closed by %s \nBYE! ;)", getName());
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void getProducingStrategy() {
        try {
            while (true) {
                System.out.println("Please select input strategy:\n1 - Simple auto producing\n2 - Read string and produce it\n3 - Exit");
                String input = reader.readLine();

                switch (input) {
                    case "1":
                        producingStrategy = ProducingStrategy.SIMPLE_PRODUCING;
                        return;
                    case "2":
                        producingStrategy = ProducingStrategy.READ_AND_PRODUCING;
                        return;
                    case "3":
                        return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startReadAndProducing(KafkaProducer<String, String> producer, BufferedReader reader) throws IOException {
        ProducerRecord<String, String> record;
        while (true) {
            String line;
            synchronized (LOCK) {
                if (closeAllThreads) {
                    break;
                }
                System.out.println("Please input line (\"exit\" for exit), (\"quit\" to close all threads): " + getName());
                line = reader.readLine();
                if ("quit".equalsIgnoreCase(line)) {
                    closeAllThreads = true;
                }
                if (closeAllThreads || "exit".equalsIgnoreCase(line)) {
                    break;
                }
            }
            record = new ProducerRecord<>(TOPIC_NAME, line);
            producer.send(record, new DemoProducerCallback());
        }
    }


    private void startSimpleProducing(KafkaProducer<String, String> producer, BufferedReader reader) throws IOException {
        ProducerRecord<String, String> record;
        for (int i = 0; i < 100; i++) {
            record = new ProducerRecord<>(TOPIC_NAME, getName(), "Line " + i);
            producer.send(record, new DemoProducerCallback());
        }
    }

    private class DemoProducerCallback implements Callback {
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if (e != null) {
                e.printStackTrace();
            } else {
                System.out.println(String.format("Topic %s ,offset %d ,partition %d ,timestamp %d",
                        recordMetadata.topic(), recordMetadata.offset(), recordMetadata.partition(),
                        recordMetadata.timestamp()));
            }
        }
    }

    private enum ProducingStrategy {
        SIMPLE_PRODUCING, READ_AND_PRODUCING
    }
}
